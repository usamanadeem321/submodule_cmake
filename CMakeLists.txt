cmake_minimum_required (VERSION 3.16.3)
project(test)


add_subdirectory(ThirdParty/benchmark)

include_directories (ThirdParty/benchmark/include)

add_executable(exefile src/introd.cpp)

target_link_directories(exefile PRIVATE benchmark)

target_link_libraries (exefile benchmark)

#This command has been used: cmake -DBENCHMARK_ENABLE_GTEST_TESTS=OFF ..